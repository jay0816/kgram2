const express = require("express")
const multer = require("multer")
//path to my public folder which contains all my client side data.
const path = "./public";
//Path( the path  to all uploaded images)
const app = express();


const fs = require("fs")

const pathReq = require("path")
//using express allows this communition with the backend.

const uploadPath = "./public/uploads/"

const storage = multer.diskStorage({
    destination: uploadPath,
    filename:function(req, file, callback){
        callback(null, file.fieldname+"-"+Date.now()+pathReq.extname(file.originalname))

    }
})

//Multer parses the binary data and saves as image//
const upload = multer({
    dest: path,
    storage: storage
});

app.use(express.json())
app.use(express.static(uploadPath))
app.use(express.static(path));

app.set('views','./views')
app.set('view engine','pug');


app.get("/", (req, res) => {
    console.log("hey")
    fs.readdir( uploadPath ,function(err, images){
        console.log(images)
        res.render("indx",{images:images.map(image => "/uploads/" + image )});
    })
});

// let images = []

app.post("/upload", upload.single("myFile"), (req, res) => {
    
            // console.log(req.file)
            res.render('upload',{image:req.file.filename})

})

//Endpoint that gets hit when we check localhost in browser  .send sends a large HTML string (where we'll write HTML tags -- form tag)
//New image that you uploaded and the back button//

app.post("/latest", (req,res) => {
    const after =req.body.after;

    fs.readdir( uploadPath, (err,items) => {
        //image array this is the container all images are pushed to for the /latest endpoint)
        let images =[];
        let timestamp = 0;
    
        //two problems to solve :
        //push to array all imageNames whose modifided time is greater than clients after time
        //    - this will be sent back to client i rsponse body porperty called images
        //find  the greatest modified tme out of all
    items.forEach(imageName => {
         // .stateSync is the status of the file you selected                                  
        let imageTime = fs.statSync( uploadPath + imageName).mtimeMs
        // console.log(fs.statSync( path +'/'+ imageName))
        //req.body.after(reg=request body= the  body of the fecth request made on the client side after= the timestamp of the uploaded image.)
        if(imageTime > req.body.after){
            images.push(imageName)
        }
        
        if(imageTime > timestamp){
            timestamp = imageTime
        }
    })
    res.send({ images,timestamp })
    // console.log(images)

});
})





                
                   
   

app.listen(3000, () => {
    console.log("Listening on Port 3000")
});




