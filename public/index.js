let timestamp = Date.now();
let errors = 0;

let intervalID = setInterval(
    polling, 5000)

const fetchLatestImages = () => {
    intervalID
}

function polling() {


    if (errors >= 2) {
        clearInterval(intervalID)
        return
    }
    fetch('/latest', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({'after':timestamp})
    }).then(res => res.json())
        .then(res => {
            timestamp = res.timestamp
            for(let image of res.images){
                let link = document.createElement('a')
                link.href = '/'+image
                let curImage = document.createElement('img')
                curImage.src = image
                link.appendChild(curImage)
                document.body.prepend(link)
            }
        })
        .catch(err => {
            console.log('ERROR')
            errors++
        })
}

fetchLatestImages();



